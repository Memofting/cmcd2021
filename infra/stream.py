import io
import typing as t
from dataclasses import dataclass, field
from math import ceil


STREAM_END = '-1'


@dataclass
class InBitStream:
    stream: t.BinaryIO
    _gen: t.Optional[t.Any] = field(default=None, init=False)

    def read(self):
        if self._gen is None:
            self._gen = self._read()
        return self._gen

    def _read(self):
        for byte in self.stream.read():
            yield from [bit for bit in bin(byte)[2:].zfill(8)]


@dataclass
class OutBitStream:
    stream: t.BinaryIO
    buf_size: int = 16
    _gen: t.Optional[t.Any] = field(default=None, init=False)

    def write(self, number: str) -> None:
        if self._gen is None:
            self._gen = self._write()
            next(self._gen)
        try:
            self._gen.send(number)
        except StopIteration:
            pass

    def __iadd__(self, other: str) -> "OutBitStream":
        self.write(other)
        return self

    def _write(self):
        buf = ''
        while True:
            bin: str = yield
            if bin == STREAM_END:
                buf = buf.ljust(8 * ceil(len(buf) / 8), '0')
                self._flush(buf, True)
                break
            else:
                buf += bin
            if len(buf) >= self.buf_size:
                buf = self._flush(buf)

    def _flush(self, buf, all: bool = False) -> bytes:
        while len(buf) >= self.buf_size or all and buf:
            write_buf, buf = buf[:self.buf_size], buf[self.buf_size:]
            for i in range(0, len(write_buf), 8):
                byte = int(write_buf[i:i + 8].zfill(8), 2)
                self.stream.write(bytes([byte]))
        return buf
