import io
import time

from encoding.decapitalizing.interface import ENCODED_DECAP_FN, DECAP_FN, DECODED_DECAP_FN
from encoding.ppm.ppm import PPM
from infra.stream import OutBitStream


def solution():
    for depth, use_mask, update_rule in [
        # (2, False, "C"),
        # (2, True, "C"),
        (2, True, "D"),
        # (3, False, "C"),
        # (3, True, "C"),
        # (3, True, "D"),
        # (4, False, "D"),
        # (4, True, "C"),
        # (4, False, "D"),
        # (5, False, "D"),
        # (5, True, "D"),
    ]:
        encode(depth, update_rule, use_mask)
        decode(depth, update_rule, use_mask)


def encode(depth, update_rule, use_mask):
    print("********")
    print(f"PPM with depth: {depth}, use mask: {use_mask}, update_rule: {update_rule}")
    enc = PPM(depth, use_mask, update_rule=update_rule)
    enc_stream = io.BytesIO()
    out_enc_stream = OutBitStream(enc_stream)
    with open(DECAP_FN, "br") as file:
        text = file.read()
    ts = time.time()
    enc.encode(text, out_enc_stream)
    enc_stream.seek(0)
    with open(ENCODED_DECAP_FN, 'wb') as file:
        file.write(enc_stream.read())
    te = time.time()
    print(f"Compression rate: {DECAP_FN.stat().st_size / ENCODED_DECAP_FN.stat().st_size}")
    print(f"Elapsed time: {te - ts}")
    print("********", end='\n')


def decode(depth, update_rule, use_mask):
    dec = PPM(depth, use_mask, update_rule=update_rule)
    dec_stream = io.BytesIO()
    out_dec_stream = OutBitStream(dec_stream)
    with open(DECAP_FN, "br") as file:
        text = file.read()
    with open(ENCODED_DECAP_FN, "br") as file:
        encoded = b''
        for byte in file.read():
            encoded += bin(byte)[2:].zfill(8).encode()
    ts = time.time()
    dec.decode(len(text), encoded, out_dec_stream)
    dec_stream.seek(0)
    decoded = dec_stream.read()
    with open(DECODED_DECAP_FN, 'wb') as file:
        file.write(decoded)
    te = time.time()
    print(f"Real text equal to decoded: {text == decoded}")
    print(f"Decoding elapsed time: {te - ts}")
    print("********", end='\n')


def main():
    depth, use_mask, update_rule = 4, True, "D"
    decode(depth, use_mask)


if __name__ == "__main__":
    main()
