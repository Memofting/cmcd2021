import io
from collections import Counter
from math import log2

from encoding.arithmetic.arithmetic import Arithmetic
from infra.stream import OutBitStream


def solution(t: bytes = b'abracadabra', n: int = 10000, k: int = 32):
    s = t * n
    enc = Arithmetic(s, k)

    print("Вариан 7. Задача 2. Сергеев Виктор Андреевич.")
    print("********************************")
    print("""
    у нас есть вероятности, построив по ним дерево хафмана:

                    +
                 1/   \\0
                A      +
                    1/   \\0
                    Б     +
                       1/   \\0
                       Р     +
                          1/   \\0
                          Д     К


    Мы получим, что для символов будут следующие длины кодов:
    A - 1
    Б - 2
    Р - 3
    Д, К - 4

    зная вероятности появления этих символов:
    A - 5/11
    Б - 2/11
    Р - 2/11
    Д - 1/11
    К - 1/11

    Посчитаем матожидание:
    1 * 5/11 + 2 * 2/11 + 3 * 2/11 + 4 * 1/11 + 4 * 1/11 = 2.09
    """)
    print("********************************")
    stream = io.BytesIO()
    out = OutBitStream(stream)
    enc.encode(out)
    stream.seek(0)
    text = stream.read()
    expectation = len(text * 8) / len(s)
    print("""
    Здесь мы берем длину закодированного текста и делим на длину исходного текста.
    Так мы получим среднюю длину закодированного символа.
    """)
    print("********************************")

    counter = Counter(s)
    length = sum(counter.values())
    prob = [(symbol, count / length) for symbol, count in counter.items()]

    print("Вероятность символов:", prob)
    print("Энтропия:", sum(p * log2(1/p) for _, p in prob))
    print("Средняя длина символа для арифметического кодирования:", expectation)
    print("Средняя длина символа для кодирования хаффмана: 2.09")
    print("********************************")
    print("""
    Мы видим, что ожидаемая длина символа для арифметического кодирования 
    лучше ожидаемой длины для хаффмана и лучше приближена к энтропии.
    """)
