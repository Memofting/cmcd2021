from collections import Counter
from difflib import unified_diff
from itertools import islice, chain

from encoding.decapitalizing import ChunkDecapitalizer, CAP_FN, FN, MASK_FN, DECAP_FN
from encoding.decapitalizing.dist import DistDecapitalizer
from encoding.symbol.delta import Delta
from encoding.symbol.overflow import Overflow, OverflowOther
from encoding.symbol.gamma import Gamma


def solution():
    print("Первая часть. Декапитализация. Сейчас мы сравним несколько алгоритмов.")
    for decapitalizer in [
        DistDecapitalizer(Gamma(1)),
        DistDecapitalizer(Delta(0)),
        DistDecapitalizer(Overflow(1)),
        DistDecapitalizer(OverflowOther()),
        ChunkDecapitalizer()
    ]:
        decapitalizer.encode()
        decapitalizer.decode()
        print("****************")
        with open(CAP_FN) as cap_fn, open(FN) as fn:
            first_ten_diff = ''.join(islice(unified_diff(fn.readlines(), cap_fn.readlines()), 10))
            print(f"Декапитализатор: {decapitalizer}")
            if first_ten_diff:
                print(
                    f"Разница в изначальном и закодированном->раскодированном тексте {decapitalizer}: {first_ten_diff}")
            else:
                decapitalizer.encode()
                meta_size = MASK_FN.stat().st_size
                text_size = FN.stat().st_size
                print(f"{'Размер исходного файла:':<50} {text_size:>20} байт")
                print(f"{'Размер доп информации для капитализации:':<50} {meta_size:>20} байт")
                print(f"{'В среднем битов на символ исходного текста:':<50} {8 * meta_size} / {text_size} = {8 * meta_size / text_size:.3}")
            print('**************')
    print()
    print("""
        Выгоднее всего получилось сжать двумя алгоритмами:
        
        Первый - когда мы каждые 256 символов текста записываем 0 и записываем в маску индекс заглавной буквы по
        модулю 256. Потом мы к этому файлу применяем статическое арифметическое кодирование. И с начала файла 
        записываем всю необходимую информацию о сегментах. Арифметический энкодер запускался с параметром N=32.
        
        Второй - когда вычисляем разницу между соседними заглавными буквами и записываем эти числа в файл, закодировав
        их с переполнением по схеме 8 + 12. 12 - потому что максимальное расстояние чуть больше чем 2048.
        
        Удивительно, что арифметика хорошо сжала, потому что распределения чисел вроде должны быть примерно одинаковы.
        А вот то, что кодирование с переполнением сжало хорошо - не удивительно. Потому что расстояние между заглавными 
        буквами не часто будет очень большим. 
        
        Версию с антисловарём я не реализовал, так как сосредоточился на реализации кодирований и их перебора над 
        обычным массивом разниц.  
    """)
    print()
    print("**************")
    print()
    print("""
        Сейчас мы посчитаем количество контекстных моделей порядка три:
    """)
    with open(DECAP_FN) as fn:
        text = fn.read()
        counter_three = Counter(''.join(_tuple) for _tuple in zip(text, text[1:], text[2:]))
        counter_four = Counter(''.join(_tuple) for _tuple in zip(text, text[1:], text[2:], text[3:]))
        contexts = {context: Counter() for context in counter_three.keys()}
        for four_context, count in counter_four.items():
            contexts[four_context[:3]].update({four_context[-1]: count})

        most_common_three = ', '.join(
            repr(context) + f" {count}" for context, count in counter_three.most_common(5))
        counters = ', '.join(
            map(str, sorted(chain.from_iterable(context.values() for context in contexts.values()))[-5:][::-1]))
        print(f"{'Пять наиболее частых контекстов порядка 3':<50} {most_common_three:>20}")
        print(f"{'Пять наибольших счётчиков символа после контекста':<50} {counters:>20}")

        print()
        print(f'{"Для описания подстроки контекста (три символа):":<50} {"24 бита":>21}')
        print('# ceil(log2(27365)) = 15')
        print(f'{"Для счётчика вхождения контекста в текст хватит: ":<50}' + f"{'15 бит':>21}")
        print(f'{"Для счётчика количества символов после контекста:":<50} {"8 битов":>21}')
        print(f"{'Общее количество тройных контекстов':<50} {len(counter_three):>20}")
        print(f"{'Потребляемая память':<50} {'memory = (24 + 15 + 8) * 8200'}")

        print()
        print('# ceil(log2(19710)) = 15')
        print(f'{"Для счётчика количества символа после контекста:":<50} {"15 битов":>21}')
        print(f'{"Для символа после контекста":<50} {"8 битов":>21}')
        print(f"{'Потребляемая память':<50} {'memory += (количество символов после контекста) * (8 + 15) # для каждого контекста'}")

        memory = (24 + 15 + 8) * len(counter_three)
        for counter in contexts.values():
            memory += len(counter) * (8 + 15)

        print()
        print(f"{'Потребляемая память':<50} {memory // 8} байт")
        print(f"{'Исходный текст':<50} {CAP_FN.stat().st_size} байт")
        fraction = 8 * memory // 8 / CAP_FN.stat().st_size
        print(f"Пропорция потребляемой памяти для построения контекстной модели и памяти текста: {memory // 8} / {CAP_FN.stat().st_size} = {fraction:.3}")

        # тут считаем все контексты порядка 3
        print("""
        Я рассмотрел все тройки символов в декапитализированном тексте и посчитал статистику их встречаемости и 
        статистику символов, которые идут после них. Потом проанализировав максимальные значения этих статистик
        я посчитал объем памяти в битах, который бы занимал один контекст и просуммировал по всем контекстам.
        
        Здесь я не использовал никакое кодирование у символов, кроме их обычного бинарного представления в памяти.
        Я предполагал что все данные будут находиться в памяти линейно и непрерывно, так я посчитал потребляемую память.
        
        Память, которая занимает эта модель равна, примерно, 1/10 от декапитализированного текста.
        """)
