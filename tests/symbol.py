import io
import typing as t
from itertools import product

import pytest

from encoding.symbol.delta import Delta
from encoding.symbol.interface import SymbolEncoding
from encoding.symbol.gamma import Gamma
from encoding.symbol.overflow import Overflow
from infra.stream import InBitStream, OutBitStream


@pytest.mark.parametrize(
    "numbers,encoder",
    product(
        [[0], [1], [2], [3], [4], [16], [17], [18], [200], [1000], [9983]],
        [Gamma(0), Delta(0), Overflow(0)]
    )
)
def test_encoding_decoding_through_file(numbers: t.List[int], encoder: SymbolEncoding):
    stream = io.BufferedRandom(io.BytesIO())

    out_stream = OutBitStream(stream)
    for number in numbers:
        encoded = encoder.encode(number)
        out_stream.write(encoded)
    out_stream.write('-1')
    stream.seek(0)
    in_stream = InBitStream(stream)
    decoded_numbers = [encoder.decode(in_stream) for _ in numbers]

    assert numbers == decoded_numbers, f"numbers not equal: {numbers} != {decoded_numbers}"


@pytest.mark.parametrize(
    'number,code',
    [
        (1, '1'),
        (2, '0100'),
        (3, '0101'),
        (4, '01100'),
        (5, '01101'),
        (6, '01110'),
        (7, '01111'),
        (8, '00100000'),

    ]
)
def test_delta(number, code):
    encoded = Delta().encode(number)

    assert encoded == code, f"{encoded} != {code}"

    # stream = io.BufferedRandom(io.BytesIO())
    # stream.write(encoded.encode())
    # stream.seek(0)
    #
    # print(stream.read())
    # stream.seek(0)
    #
    # decoded = Delta().decode(stream)
    #
    # assert decoded == number, f"{decoded} != {number}"
