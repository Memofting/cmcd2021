import collections
import itertools
import json
import math
import typing as t
import io

import pytest

from encoding.arithmetic.arithmetic import Arithmetic
from encoding.ppm.contextnode import BITS
from encoding.ppm.ppm import PPM
from infra.stream import OutBitStream


@pytest.mark.parametrize(
    "text, depth, update_rule",
    list(itertools.product(
        [
            b"aaaabbbbbccccccddddddd",
            b"abba",
            b"accaccggacca",
            b"aaaaaaaaaaaa",
            b"abba bba abb aba",
            b"arcadiaarcadia",
            b"abcdef" * 10,
            b"edv " * 100,
            b"a ab abc abd abe abf abg abh aba abs abd abf abg abh abj abk abl ab; ab'",
            b"""  when alcibiades had finished, there was a laugh at his
outspokenness; for he seemed to be still in love with socrates. you
are sober, alcibiades, said socrates, or you would never have gone
so far about to hide the purpose of your satyr's praises, for all this
long story is only an ingenious circumlocution, of which the point
comes in by the way at the end; you want to get up a quarrel between
me and agathon, and your notion-is that i ought to love you and nobody
else, and that you and you only ought to love agathon. but the plot of
this satyric or silenic drama has been detected, and you must not
allow him, agathon, to set us at variance.
  i believe you are right, said agathon, and i am disposed to think
that his intention in placing himself between you and me was only to
divide us; but he shall gain nothing by that move; for i will go and
lie on the couch next to you.
  yes, yes, replied socrates, by all means come here and lie on the
couch below me.
  alas, said alcibiades, how i am fooled by this man; he is determined
to get the better of me at every turn. i do beseech you, allow agathon
to lie between us.
  certainly not, said socrates, as you praised me, and i in turn ought
to praise my neighbour on the right, he will be out of order in
praising me again when he ought rather to be praised by me, and i must
entreat you to consent to this, and not be jealous, for i have a great
desire to praise the youth.
  hurrah! cried agathon, i will rise instantly, that i may be
praised by socrates.
  the usual way, said alcibiades; where socrates is, no one else has
any chance with the fair; and now how readily has he invented a
specious reason for attracting agathon to himself.
  agathon arose in order that he might take his place on the couch
by socrates, when suddenly a band of revellers entered, and spoiled
the order of the banquet. some one who was going out having left the
door open, they had found their way in, and made themselves at home;
great confusion ensued, and every one was compelled to drink large
quantities of wine. aristodemus said that eryximachus, phaedrus, and
others went away-he himself fell asleep, and as the nights were long
took a good rest: he was awakened towards daybreak by a crowing of
cocks, and when he awoke, the others were either asleep, or had gone
away; there remained only socrates, aristophanes, and agathon, who
were drinking out of a large goblet which they passed round, and
socrates was discoursing to them. aristodemus was only half awake, and
he did not hear the beginning of the discourse; the chief thing
which he remembered was socrates compelling the other two to
acknowledge that the genius of comedy was the same with that of
tragedy, and that the true artist in tragedy was an artist in comedy
also. to this they were constrained to assent, being drowsy, and not
quite following the argument. and first of all aristophanes dropped
off, then, when the day was already dawning, agathon. socrates, having
laid them to sleep, rose to depart; aristodemus, as his manner was,
following him. at the lyceum he took a bath, and passed the day as
usual. in the evening he retired to rest at his own home."""
        ],
        [
            3
        ],
        [
            "C",
            "D"
        ]
    ))

)
def test_ppm_encode_graph(text: bytes, depth: int, update_rule: str):
    enc, dec = PPM(depth, update_rule=update_rule), PPM(depth, update_rule=update_rule)
    arithmetic = Arithmetic(text, BITS)

    ath_stream = io.BytesIO()
    out_ath_stream = OutBitStream(ath_stream)
    enc_stream, dec_stream = io.BytesIO(), io.BytesIO()
    out_enc_stream, out_dec_stream = OutBitStream(enc_stream), OutBitStream(dec_stream)

    enc.encode(text, out_enc_stream)
    enc_stream.seek(0)

    encoded = b''
    for byte in enc_stream.read():
        encoded += bin(byte)[2:].zfill(8).encode()

    dec.decode(len(text), encoded, out_dec_stream)
    dec_stream.seek(0)

    assert str(enc.lambda_context) == str(dec.lambda_context)

    decoded = dec_stream.read()

    arithmetic.encode(out_ath_stream)
    ath_stream.seek(0)

    ath_encoded = b""
    for byte in ath_stream.read():
        ath_encoded += bin(byte)[2:].zfill(8).encode()

    entropy = sum(p * math.log2(1 / p) for p in (c / len(text) for c in collections.Counter(text).values()))
    print(
        'ENTROPY: ', entropy,
        'PPM:', len(text) / len(encoded),
        'Arithmetic: ',len(text) / len(ath_encoded))
    print()

    print(enc.lambda_context.coder.arithmetic.segments.borders(97))

    assert text == decoded, f"\noriginal: {text}\ndec: {decoded}"
