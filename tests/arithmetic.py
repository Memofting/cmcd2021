import io

import pytest

from encoding.arithmetic.arithmetic import Arithmetic
from infra.stream import OutBitStream


@pytest.mark.parametrize(
    'text,N',
    [
        (b'a', 1000),
        (b'ab', 1000),
        (b'abb', 1000),
        (b'abbc', 1000),
        (b'abbc', 1000),
        (b'abbcc', 1000),
        (b'abbccc', 1000),
        (b'abrakadabra', 1000),
        (b'acagaatagaga', 1000),
    ]
)
def test_static_arithmetic(text: bytes, N: int):
    s = text * N
    enc = Arithmetic(s, 32)
    dec = Arithmetic(s, 32)
    dec.length = len(s)

    decode_encode_assert(dec, enc, s)


@pytest.mark.parametrize(
    'text,N,static_cube',
    [
        *[(i.to_bytes(1, 'big'), 10, True) for i in range(256)],
        *[(i.to_bytes(1, 'big'), 10, False) for i in range(256)],
        (b'a', 100, False),
        (b'a', 100, True),
        (b'ab', 1000, False),
        (b'abb', 1000, False),
        (b'abbc', 1000, False),
        (b'abbc', 1000, False),
        (b'abbcc', 1000, False),
        (b'abbccc', 1000, False),
        (b'abrakadabra', 1000, False),
        (b'acagaatagaga', 1000, False),
        (b'\x00\x00\xff', 1, False),
    ]
)
def test_dynamic_arithmetic(text: bytes, N: int, static_cube: bool):
    s = text * N
    enc = Arithmetic(s, 32, False, static_cube)
    dec = Arithmetic(None, 32, False, static_cube)
    dec.length = len(s)
    decode_encode_assert(dec, enc, s)


def decode_encode_assert(dec, enc, s):
    enc_stream, dec_stream = io.BytesIO(), io.BytesIO()
    out_enc_stream, out_dec_stream = OutBitStream(enc_stream), OutBitStream(dec_stream)

    for symbol in s:
        enc.encode_symbol(symbol, out_enc_stream)
    enc.encode_finish(out_enc_stream)

    enc_stream.seek(0)
    encoded = b''
    for byte in enc_stream.read():
        encoded += bin(byte)[2:].zfill(8).encode()

    for _ in range(len(s)):
        dec.decode_symbol(encoded, out_dec_stream)
    dec.decode_finish(out_dec_stream)

    dec_stream.seek(0)
    decoded = dec_stream.read()
    assert s == decoded, f"\nenc: {s}\ndec: {decoded}"
