
С ростом глубины контекста увеличивается качество сжатия но также увеличивается время работы.

Стоит отметить, что энтропия у этого текста равна:
    4.271

То есть энтропийное сжатие (например, арифметикой) может максимум сжать текст в:
    8 / 4.271 = 1.873
раз.

Экспериментальное сравнение настроек алгоритма:

Время работы не точное а приблизительное, можно видеть как маскировка для
больших контекстов сильно не влияет на время работы, а вот степень сжатия увеличивает.
Такое различное время в работе с разными правилами обновления объясняется тем, что
эксперименты запускались в разное время. Для одновременного запуска время работы не
слишком различается.

Для глубины 4:

    PPM with depth: 4, use mask: False, update_rule: C
    Compression rate: 3.920422232185575
    Elapsed time: 775.4158418178558

    PPM with depth: 4, use mask: False, update_rule: D
    Compression rate: 3.9334299429399513
    Elapsed time: 538.0947623252869

    PPM with depth: 4, use mask: True, update_rule: C
    Compression rate: 4.074754020428971
    Elapsed time: 794.9493989944458

    PPM with depth: 4, use mask: True, update_rule: D
    Compression rate: 4.110791035498945
    Elapsed time: 638.1016519069672

Для глубины 5:

    PPM with depth: 5, use mask: False, update_rule: C
    Compression rate: 3.85113565153647
    Elapsed time: 1088.497323513031

    PPM with depth: 5, use mask: False, update_rule: D
    Compression rate: 3.8459744599453303
    Elapsed time: 827.77885222435

    PPM with depth: 5, use mask: True, update_rule: C
    Compression rate: 4.163667972669807
    Elapsed time: 1388.627010345459

    PPM with depth: 5, use mask: True, update_rule: D
    Compression rate: 4.210140244296516
    Elapsed time: 867.1064009666443

Получается, что предположение о том, что для текста правило обновления D подходит
лучше чем C - подтвердилось. С ростом длины контекста у меня очень сильно увеличивалось
потребление памяти, контексты длины 6 я не смог даже запустить. Также декодер работает медленнее.
Вот пример запуска кодера и декодера на контекстах длины 2:

    PPM with depth: 2, use mask: True, update_rule: D
    Compression rate: 3.0096952812601785
    Elapsed time: 412.98381185531616

    Real text equal to decoded: True
    Decoding elapsed time: 711.0991771221161

Тут видно, как время работы разительно увеличилось для декодера, относительно кодера.
Запуск происходил в одно время.
