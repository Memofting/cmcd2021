from dataclasses import dataclass, field


@dataclass
class ArithmeticContext:
    low: int = field(init=False, default=0)
    high: int
    bits: int = field(init=False, default=0)
    shift: int = field(init=False, default=0)

    @property
    def diff(self) -> int:
        return self.high - self.low

    def __str__(self) -> str:
        return f"{self.low} {self.high} {self.bits} {self.shift}"

    def __repr__(self) -> str:
        return self.__str__()
