import contextlib
import copy
import typing as t
from dataclasses import dataclass, field

from encoding.arithmetic.context import ArithmeticContext
from encoding.arithmetic.segments import Segments, StaticSegments, ADD_SYMBOL
from encoding.symbol.interface import YASing
from infra.stream import OutBitStream, STREAM_END


@dataclass
class Arithmetic:
    text: t.Optional[bytes] = None
    n: int = 32
    static_text: bool = True
    static_cube: bool = False
    use_dynamic_alphabet: bool = True
    update_rule: str = "C"
    length: int = field(init=False)

    segments: Segments = field(init=False)
    static_segments: StaticSegments = field(init=False)

    context: ArithmeticContext = field(init=False)

    def __post_init__(self) -> None:
        self.static_segments = StaticSegments(self.n)
        if self.static_text and self.text is not None:
            self.segments = Segments(self.n, self.text, update_rule=self.update_rule)
        else:
            self.segments = Segments(self.n, update_rule=self.update_rule)
        self.context = ArithmeticContext(self.segments.power - 1)

    @contextlib.contextmanager
    def mask(self, symbols: t.Set[str]) -> None:
        segments = self.static_segments if self.static_cube else self.segments
        counts = copy.deepcopy(segments.counts)
        c = 1 if self.update_rule == "C" else 0.5
        # c = 1
        # reader_count = len(symbols)
        for symbol in symbols:
            segments.counts.add(symbol, -counts[symbol])
            segments.reader_count -= counts[symbol] + (1 - c)
        if not self.static_cube:
            segments.counts.add(ADD_SYMBOL, - c * len(symbols))
        yield
        for symbol in symbols:
            segments.counts.add(symbol, counts[symbol])
            segments.reader_count += counts[symbol] + (1 - c)
        if not self.static_cube:
            segments.counts.add(ADD_SYMBOL, c * len(symbols))

    def encode(self, stream: OutBitStream) -> None:
        for c in self.text:
            self.encode_symbol(c, stream)
        self.encode_finish(stream)

    def encode_symbol(self, c: int, stream: OutBitStream) -> None:
        if self.static_cube:
            self.static_segments.update_l_h(c, self.context)
            self._encode_symbol(stream)
        # elif not self.segments.contains(c):
        #     self.__encode_symbol(ADD_SYMBOL, stream)
        #
        #     self.static_segments.update_l_h(c, self.context)
        #     self._encode_symbol(stream)
        #     self.segments.update(c)
        else:
            self.__encode_symbol(c, stream)

    def __encode_symbol(self, c: int, stream: OutBitStream) -> None:
        self.segments.update_l_h(c, self.context)
        self._encode_symbol(stream)
        if self.use_dynamic_alphabet:
            self.segments.update(c)

    def encode_finish(self, stream: OutBitStream):
        bl = bin(self.context.low)[2:].zfill(self.n)
        stream += bl[0] + '1' * self.context.bits + bl[1:]
        stream += STREAM_END

    def decode(self, encoded: bytes, stream: OutBitStream) -> None:
        for _ in range(self.length):
            self.decode_symbol(encoded, stream)
        self.decode_finish(stream)

    def decode_finish(self, stream):
        stream += STREAM_END

    def decode_symbol(self, encoded: bytes, stream: OutBitStream) -> int:
        if self.static_cube:
            w = self.get_w(encoded)
            c = self.static_segments.get_c(w, self.context)
            # TODO: WTF!? почему строку снизу надо было добавить...
            self.static_segments.update_l_h(c, self.context)
            self._decode_symbol()
            assert self.context.low < self.context.high, "impossible decoding"
            stream += bin(c)[2:].zfill(8)
            return c
        c = self.segments.get_c(self.get_w(encoded), self.context)
        self.segments.update_l_h(c, self.context)
        self._decode_symbol()
        if self.use_dynamic_alphabet:
            self.segments.update(c)
        if c == ADD_SYMBOL and self.use_dynamic_alphabet:
            c = self.static_segments.get_c(self.get_w(encoded), self.context)
            self.static_segments.update_l_h(c, self.context)
            self._decode_symbol()
            self.segments.update(c)
        if c != ADD_SYMBOL:
            stream += bin(c)[2:].zfill(8)
        return c

    def _encode_symbol(self, stream: OutBitStream) -> None:
        r1, r2 = 0, 0
        bl, bh = bin(self.context.low)[2:].zfill(self.n), bin(self.context.high)[2:].zfill(self.n)
        while r1 < min(len(bl), len(bh)) and bl[r1] == bh[r1]:
            r1 += 1
        if r1 > 0:
            if self.context.bits > 0:
                stream += bl[0] + ("0" if bl[0] == "1" else "1") * self.context.bits + bl[1:r1]
                self.context.shift += self.context.bits
                self.context.bits = 0
            else:
                stream += bl[:r1]
            self.context.low = int(bl[r1:] + "0" * r1, 2)
            self.context.high = int(bh[r1:] + "1" * r1, 2)
            self.context.shift += r1
        bl, bh = bin(self.context.low)[2:].zfill(self.n), bin(self.context.high)[2:].zfill(self.n)
        while (bl[0] == "0" and bh[0] == "1"
               and r2 + 1 < min(len(bl), len(bh))
               and bl[r2 + 1] == '1' and bh[r2 + 1] == '0'):
            r2 += 1
        if r2 > 0 and self.context.bits == 0:
            self.context.low = int(bl[:1] + bl[1 + r2:] + "0" * r2, 2)
            self.context.high = int(bh[:1] + bh[1 + r2:] + "1" * r2, 2)
            self.context.bits = r2

    def _decode_symbol(self) -> None:
        r1, r2 = 0, 0
        bl, bh = bin(self.context.low)[2:].zfill(self.n), bin(self.context.high)[2:].zfill(self.n)
        while r1 < min(len(bl), len(bh)) and bl[r1] == bh[r1]:
            r1 += 1
        if r1 > 0:
            self.context.low = int(bl[r1:] + "0" * r1, 2)
            self.context.high = int(bh[r1:] + "1" * r1, 2)
            self.context.shift += r1 + self.context.bits
            self.context.bits = 0
        bl, bh = bin(self.context.low)[2:].zfill(self.n), bin(self.context.high)[2:].zfill(self.n)
        while (bl[0] == "0" and bh[0] == "1"
               and r2 + 1 < min(len(bl), len(bh))
               and bl[r2 + 1] == '1' and bh[r2 + 1] == '0'):
            r2 += 1
        if r2 > 0 and self.context.bits == 0:
            self.context.low = int(bl[:1] + bl[1 + r2:] + "0" * r2, 2)
            self.context.high = int(bh[:1] + bh[1 + r2:] + "1" * r2, 2)
            self.context.bits = r2

    def get_w(self, encoded):
        if self.context.shift > len(encoded):
            w_bin = b'0'
        elif self.context.shift + 1 + self.context.bits > len(encoded):
            w_bin = encoded[self.context.shift:self.context.shift + 1]
        else:
            w_bin = encoded[self.context.shift:self.context.shift + 1]
            w_bin += encoded[
                     self.context.shift + 1 + self.context.bits: self.context.shift + self.context.bits + self.n]
        w = int(w_bin.ljust(self.n, b'0'), 2)
        return w


class YASingArithmetic(YASing):
    arithmetic: Arithmetic
    context: ArithmeticContext

    def __init__(self, bits: int, static_cube: bool, update_rule: str = "C"):
        self.arithmetic = Arithmetic(
            n=bits,
            static_text=False,
            static_cube=static_cube,
            update_rule=update_rule,
            use_dynamic_alphabet=False
        )
        self.context = self.arithmetic.context

    def encode(self, symbol: int, stream: OutBitStream) -> None:
        self.arithmetic.context = self.context
        self.arithmetic.encode_symbol(symbol, stream)
        self.context = self.arithmetic.context

    def encode_finish(self, stream: OutBitStream) -> None:
        self.arithmetic.context = self.context
        self.arithmetic.encode_finish(stream)
        self.context = self.arithmetic.context

    def decode(self, encoded: bytes, stream: OutBitStream) -> int:
        self.arithmetic.context = self.context
        ret = self.arithmetic.decode_symbol(encoded, stream)
        self.context = self.arithmetic.context
        return ret

    def decode_finish(self, stream: OutBitStream) -> None:
        self.arithmetic.context = self.context
        self.arithmetic.decode_finish(stream)
        self.context = self.arithmetic.context

    @contextlib.contextmanager
    def use_context(self, context: t.List[ArithmeticContext]):
        self.context = context[0]
        yield
        context[0] = self.context

    @contextlib.contextmanager
    def use_mask(self, mask: t.Set[str]):
        with self.arithmetic.mask(mask):
            yield
