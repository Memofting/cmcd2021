import typing as t
from fenwick import FenwickTree
from collections import Counter
from math import ceil

from encoding.arithmetic.context import ArithmeticContext

TSegments = t.List[t.Optional[t.Tuple[int, int]]]
ADD_SYMBOL = 256


class Segments:
    bits: int
    power: int
    reader_count: int
    counts: FenwickTree
    alphabet_size: int
    update_rule: str
    _update_count: float
    _use_static_segments: bool
    _segments: TSegments

    def __init__(
            self, bits: int = 32, text: t.Optional[bytes] = None, update_rule: str = "C",
            use_dynamic_alphabet: bool = True
    ) -> None:
        self.update_rule = update_rule
        self._update_count = 1 if self.update_rule == "C" else 0.5
        self.bits = bits
        self.power = 2 ** self.bits
        self._use_static_segments = text is not None
        if text is not None:
            self.alphabet_size = 256
            counts = [0 for _ in range(self.alphabet_size)]
            for symbol, count in Counter(text).items():
                counts[symbol] = count
            self.counts = FenwickTree(self.alphabet_size)
            self.counts.init(counts)
            self._set_segments_from_count()
        else:
            self.alphabet_size = 257
            self.counts = FenwickTree(self.alphabet_size)
            self.counts.init([0 for _ in range(self.alphabet_size)])
            # self.counts[ADD_SYMBOL] = self._update_count
        self.reader_count = self.counts.prefix_sum(ADD_SYMBOL)

    def update(self, symbol: int) -> None:
        if self._use_static_segments:
            return
        self.counts.add(symbol, 1)
        self.reader_count += 1

    def symbols(self) -> t.Set[int]:
        return set(c for c, v in enumerate(self.counts.frequencies()) if v > 0)

    @property
    def Symbols(self):
        return self.counts.frequencies()

    def update_new_symbol(self, symbol: int) -> None:
        if self._use_static_segments:
            return
        self.counts.add(ADD_SYMBOL, self._update_count)
        self.counts.add(symbol, self._update_count)
        self.reader_count += 1

    def contains(self, symbol: int) -> bool:
        if self._use_static_segments:
            return True
        count = self.counts[symbol]
        return count > 0

    def borders(self, symbol: int) -> t.Tuple[int, int]:
        if self._use_static_segments:
            return self._segments[symbol]
        prob, cur_prob = 0, 1 if symbol == ADD_SYMBOL else 0
        c = self.reader_count
        if self.update_rule == "C":
            c += self.counts[ADD_SYMBOL]
        if self.reader_count != 0:
            if symbol != 0:
                prob = self.counts.prefix_sum(symbol) / c
            cur_prob = self.counts[symbol] / c
        assert prob + cur_prob <= 1
        left = ceil(self.power * prob)
        right = ceil(self.power * (prob + cur_prob)) - 1
        return left, right

    def _set_segments_from_count(self) -> None:
        self._segments = [None for _ in range(self.alphabet_size)]
        self.reader_count = self.counts.range_sum(0, self.alphabet_size)
        end_probability = 0
        for symbol, count in enumerate(self.counts):
            probability = count / self.reader_count
            self._segments[symbol] = (
                ceil(self.power * end_probability), ceil(self.power * (end_probability + probability)) - 1
            )
            end_probability += probability

    def get_c(self, w: int, context: ArithmeticContext) -> int:
        nw = min(self.power - 1, ceil((w - context.low) / context.diff * self.power))
        for symbol in range(self.alphabet_size):
            left, right = self.borders(symbol)
            if nw <= right:
                return symbol
        raise ValueError

    def update_l_h(self, symbol: int, context: ArithmeticContext) -> None:
        symbol_left, symbol_right = self.borders(symbol)
        context.low, context.high = (
            context.low + ceil(symbol_left * (context.diff + 1) / self.power),
            context.low + ceil((symbol_right + 1) * (context.diff + 1) / self.power) - 1
        )


class StaticSegments(Segments):
    def __init__(self, bits: int = 32) -> None:
        super().__init__(bits)
        self.alphabet_size = 256
        self.counts = FenwickTree(self.alphabet_size)
        self.counts.init([1 for _ in range(self.alphabet_size)])
        self._set_segments_from_count()

    def borders(self, symbol: int) -> t.Tuple[int, int]:
        return self._segments[symbol]
