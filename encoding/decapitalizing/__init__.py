from .chunk import ChunkDecapitalizer
from .interface import FN, CAP_FN, DECAP_FN, MASK_FN

__all__ = [
    ChunkDecapitalizer
]