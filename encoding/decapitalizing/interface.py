import os
import string
import typing as t
from copy import copy
from pathlib import Path

dir_path = os.path.dirname(os.path.realpath(__file__))
zeros = [0 for _ in range(256)]
special_symbols = copy(zeros)
ascii_uppercase = copy(zeros)

FN = Path(f'{dir_path}/../../data/plato.txt')
CAP_FN = Path(f'{dir_path}/../../data/cap.plato.txt')
DECAP_FN = Path(f'{dir_path}/../../data/dec.plato.txt')
MASK_FN = Path(f'{dir_path}/../../data/mask.txt')
ENCODED_DECAP_FN = Path(f'{dir_path}/../../data/dec.plato.encoded.txt')
DECODED_DECAP_FN = Path(f'{dir_path}/../../data/dec.plato.decoded.txt')

for c in string.punctuation + string.whitespace + string.digits:
    special_symbols[ord(c)] = 1
for c in string.ascii_uppercase:
    ascii_uppercase[ord(c)] = 1


class Decapitalizer(t.Protocol):
    def encode(self) -> None:
        ...

    def decode(self) -> None:
        ...

    def __str__(self) -> str:
        ...

    def __repr__(self) -> str:
        return self.__str__()
