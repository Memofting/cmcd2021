from encoding.arithmetic.arithmetic import Arithmetic
from encoding.decapitalizing.interface import ascii_uppercase, FN, CAP_FN, DECAP_FN, MASK_FN, Decapitalizer
from infra.stream import OutBitStream


class ChunkDecapitalizer(Decapitalizer):
    def encode(self) -> None:
        with open(FN, 'br') as file, open(DECAP_FN, 'wb') as decap_file, open(MASK_FN, 'wb') as mask_file:
            txt_bytes = file.read()
            counter = 1
            for i, byte in enumerate(txt_bytes, start=1):
                if counter == 256:
                    mask_file.write(b'\x00')
                    counter = 1
                if ascii_uppercase[byte]:
                    byte = ord(b'a') + byte - ord(b'A')
                    mask_file.write(bytes([counter]))
                decap_file.write(bytes([byte]))
                counter += 1
        # header = 0
        with open(MASK_FN, 'rb') as mask_file:
            enc = Arithmetic(mask_file.read(), 32, False)
        # enc.encode(out_enc_stream)
        # enc_stream.seek(0)
        # encoded = b''
        # for byte in enc_stream.read():
        #     encoded += bin(byte)[2:].zfill(8).encode()
        # dec.decode(encoded, out_dec_stream)
        # dec_stream.seek(0)
        # decoded = dec_stream.read()
        file_size = MASK_FN.stat().st_size.to_bytes(2, 'big')
        with open(MASK_FN, 'wb') as mask_file:
            mask_file.write(file_size)
            out = OutBitStream(mask_file)
            enc.encode(out)
            # for i in range(0, len(encoded), 8):
            #     byte = int(encoded[i:i+8].zfill(8), 2)
            #     mask_file.write(bytes([byte]))

    def decode(self) -> None:
        dec = Arithmetic(None, 32, False)
        with open(MASK_FN, 'rb') as mask_file:
            dec.length = int.from_bytes(mask_file.read(2), 'big')
            encoded = ''
            for byte in mask_file.read():
                encoded += bin(byte)[2:].zfill(8)
            encoded = encoded.encode()
        with open(MASK_FN, 'wb') as mask_file:
            out = OutBitStream(mask_file)
            dec.decode(encoded, out)
        with open(CAP_FN, 'wb') as cap_file, open(DECAP_FN, 'rb') as decap_file, open(MASK_FN, 'rb') as mask_file:
            txt_bytes = decap_file.read()
            next_shift, next_cap = 0, 0
            for i, byte in enumerate(txt_bytes, start=1):
                while True:
                    if i <= next_cap:
                        break
                    buf = mask_file.read(1)
                    if not buf:
                        break
                    count = buf[0]
                    if count == 0:
                        next_shift += 255
                    else:
                        next_cap = next_shift + count
                        break
                if i == next_cap:
                    byte = ord(b'A') + byte - ord(b'a')
                cap_file.write(bytes([byte]))

    def __str__(self) -> str:
        return "0 на каждые 256 байт c динамической арифметикой"
