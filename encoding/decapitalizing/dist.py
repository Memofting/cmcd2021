from dataclasses import dataclass

from encoding.decapitalizing.interface import ascii_uppercase, FN, CAP_FN, DECAP_FN, MASK_FN, Decapitalizer
from encoding.symbol.interface import SymbolEncoding
from encoding.symbol.gamma import Gamma
from infra.stream import InBitStream, OutBitStream

STOP_NUMBER = 2500


@dataclass
class DistDecapitalizer(Decapitalizer):
    encoder: SymbolEncoding = Gamma(1)

    def encode(self) -> None:
        with open(FN, 'br') as file, open(DECAP_FN, 'wb') as decap_file, open(MASK_FN, 'bw') as mask_file:
            out = OutBitStream(mask_file)
            txt_bytes = file.read()
            l = -1
            for i, byte in enumerate(txt_bytes, start=1):
                if ascii_uppercase[byte]:
                    if l == -1:
                        l = cmd = i
                    else:
                        cmd = i - l
                        l = i
                    # cmd from 0
                    out.write(self.encoder.encode(cmd))
                    byte = ord(b'a') + byte - ord(b'A')
                decap_file.write(bytes([byte]))
            out.write(self.encoder.encode(STOP_NUMBER))
            out.write('-1')

    def decode(self) -> None:
        with open(DECAP_FN, 'rb') as decap_file, open(MASK_FN, 'rb') as mask_file, open(CAP_FN, 'wb') as cap_file:
            bit_in = InBitStream(mask_file)
            txt_bytes = decap_file.read()
            next_cap = self.encoder.decode(bit_in)
            for i, byte in enumerate(txt_bytes, start=1):
                if i == next_cap:
                    byte = ord(b'A') + byte - ord(b'a')
                    number = self.encoder.decode(bit_in)
                    if number != STOP_NUMBER:
                        next_cap += number
                    else:
                        next_cap = -1
                cap_file.write(bytes([byte]))

    def __str__(self) -> str:
        return f"Кодирование расстоянием с использованием символьного кодирования: {self.encoder}"
