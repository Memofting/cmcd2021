import json
import typing as t
from dataclasses import dataclass, field

from encoding.arithmetic.arithmetic import YASingArithmetic
from encoding.arithmetic.context import ArithmeticContext
from encoding.arithmetic.segments import ADD_SYMBOL
from infra.stream import OutBitStream

BITS = 32


@dataclass
class IContextNode(t.Protocol):
    coder: YASingArithmetic
    parent: t.Optional["IContextNode"] = None
    use_masking: bool = True
    children: t.Dict[int, "IContextNode"] = field(default_factory=dict)

    def encode(self, symbol: int, stream: OutBitStream, context: t.List[ArithmeticContext], mask: t.Set[str]) -> int:
        ...

    def decode(self, encoded: bytes, stream: OutBitStream, context: t.List[ArithmeticContext], mask: t.Set[str]) -> int:
        with self.coder.use_context(context):
            with self.coder.use_mask(mask - {ADD_SYMBOL}):
                return self.coder.decode(encoded, stream)

    def encode_finish(self, stream: OutBitStream, context: t.List[ArithmeticContext]) -> None:
        with self.coder.use_context(context):
            self.coder.encode_finish(stream)

    def decode_finish(self, stream: OutBitStream, context: t.List[ArithmeticContext]) -> None:
        with self.coder.use_context(context):
            self.coder.decode_finish(stream)

    def symbols(self) -> t.Set[int]:
        return self.coder.arithmetic.segments.symbols()

    def update(self, symbol: int, context: t.List[ArithmeticContext]) -> None:
        raise NotImplemented

    def update_new_symbol(self, symbol: int, context: t.List[ArithmeticContext]) -> None:
        raise NotImplemented

    def as_dict(self, context: str) -> t.Dict[str, t.Any]:
        raise NotImplemented

    def height(self):
        node = self
        res = 0
        while node.parent is not None:
            node = node.parent
            res += 1
        return res


@dataclass
class LambdaContextNode(IContextNode):
    def encode(self, symbol: int, stream: OutBitStream, context: t.List[ArithmeticContext], mask: t.Set[str]) -> int:
        with self.coder.use_context(context):
            with self.coder.use_mask(mask - {ADD_SYMBOL}):
                self.coder.encode(symbol, stream)
                return symbol

    def as_dict(self, _: str) -> t.Dict[str, t.Any]:
        return {
            "lambda": [chr(symbol) for symbol in self.children],
            "children": [
                child.as_dict(chr(symbol))
                for symbol, child in self.children.items()
            ]
        }

    def __str__(self) -> str:
        return json.dumps(self.as_dict(""))

    def __repr__(self) -> str:
        return self.__str__()


@dataclass
class ContextNode(IContextNode):
    def encode(self, symbol: int, stream: OutBitStream, context: t.List[ArithmeticContext], mask: t.Set[str]) -> int:
        with self.coder.use_context(context), self.coder.use_mask(mask - {ADD_SYMBOL}):
            if self.coder.arithmetic.segments.contains(symbol):
                self.coder.encode(symbol, stream)
                return symbol
            self.coder.encode(ADD_SYMBOL, stream)
            return ADD_SYMBOL

    def as_dict(self, context) -> t.Dict[str, t.Any]:
        return {
            context: {
                chr(c) if c != ADD_SYMBOL else "^": v
                for c, v in enumerate(self.coder.arithmetic.segments.counts.frequencies()) if v > 0
            },
            "children": [
                child.as_dict(chr(symbol) + context)
                for symbol, child in self.children.items()
            ]
        }

    def update(self, symbol: int, context: t.List[ArithmeticContext]) -> None:
        with self.coder.use_context(context):
            self.coder.arithmetic.segments.update(symbol)

    def update_new_symbol(self, symbol: int, context: t.List[ArithmeticContext]) -> None:
        with self.coder.use_context(context):
            self.coder.arithmetic.segments.update_new_symbol(symbol)


    def __str__(self) -> str:
        return json.dumps(self.as_dict(""))

    def __repr__(self) -> str:
        return self.__str__()
