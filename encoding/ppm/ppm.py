import typing as t
from dataclasses import dataclass, field

from encoding.arithmetic.arithmetic import YASingArithmetic
from encoding.arithmetic.context import ArithmeticContext
from encoding.arithmetic.segments import ADD_SYMBOL
from encoding.ppm.contextnode import LambdaContextNode, IContextNode, BITS, ContextNode
from infra.stream import OutBitStream

static_context_node: LambdaContextNode = LambdaContextNode(YASingArithmetic(BITS, True))


def _decode_with_node(
        encoded: bytes,
        decode_node: IContextNode,
        stream: OutBitStream,
        arithmetic_context: t.List[ArithmeticContext],
        use_mask: bool,
        mask_symbols: t.Set[str] = None
) -> t.Tuple[int, IContextNode]:
    path = [decode_node]
    update_symbol = []
    if mask_symbols is None:
        mask_symbols = set()
    while True:
        node = path[-1]
        if use_mask and mask_symbols and node.parent is not None and mask_symbols.issuperset(node.children):
            decoded_symbol = ADD_SYMBOL
        else:
            decoded_symbol = node.decode(encoded, stream, arithmetic_context, mask_symbols)

        if decoded_symbol != ADD_SYMBOL:
            symbol = decoded_symbol
            node.update(decoded_symbol, arithmetic_context)
            break
        else:
            # assert node.parent is not None, f"Parent is None while decoding"
            if use_mask:
                mask_symbols.update(
                    c for c, v in enumerate(node.coder.arithmetic.segments.counts.frequencies()) if v > 0
                )
            update_symbol.append(node)
            # if node.symbols() != {ADD_SYMBOL}:
            #     node.update(ADD_SYMBOL, arithmetic_context)
            if node.parent is None:
                symbol = static_context_node.decode(encoded, stream, arithmetic_context, mask_symbols)
                path.append(static_context_node)
                break
            else:
                path.append(node.parent)
    for node in update_symbol:
        node.update_new_symbol(symbol, arithmetic_context)
    return symbol, path[-1]


def _encode_with_node(
        symbol: int,
        encode_node: IContextNode,
        stream: OutBitStream,
        arithmetic_context: t.List[ArithmeticContext],
        use_mask: bool,
        mask_symbols: t.Set[str] = None,
) -> IContextNode:
    if mask_symbols is None:
        mask_symbols = set()
    while True:
        if use_mask and mask_symbols and encode_node.parent is not None and mask_symbols.issuperset(encode_node.children):
            encoded_symbol = ADD_SYMBOL
        else:
            encoded_symbol = encode_node.encode(symbol, stream, arithmetic_context, mask_symbols)

        if encoded_symbol == ADD_SYMBOL:
            if use_mask:
                mask_symbols.update(
                    c for c, v in enumerate(encode_node.coder.arithmetic.segments.counts.frequencies()) if v > 0
                )
            encode_node.update_new_symbol(symbol, arithmetic_context)
            # if encode_node.symbols() != {ADD_SYMBOL}:
            #     encode_node.update(ADD_SYMBOL, arithmetic_context)
            # encode_node.update(symbol, arithmetic_context)

            if encode_node.parent is None:
                encoded_symbol = static_context_node.encode(symbol, stream, arithmetic_context, mask_symbols)
                assert encoded_symbol == symbol, "static node should encode symbol"
                return static_context_node
            else:
                encode_node = encode_node.parent
        else:
            encode_node.update(symbol, arithmetic_context)
            return encode_node


def update_contexts(arithmetic_context, context, encode_node, symbol, update_node, update_rule: str):
    height_ = encode_node.height()
    prefix = context if height_ == 0 else context[:-height_]
    node = update_node
    for _symbol in prefix[::-1]:
        node.children.setdefault(_symbol, ContextNode(YASingArithmetic(BITS, False, update_rule), node))
        node = node.children[_symbol]
        node.update_new_symbol(symbol, arithmetic_context)
    # TODO: add difference test
    if set(update_node.symbols()) == {symbol, ADD_SYMBOL}:
        node = update_node.parent
        while node is not None and set(node.symbols()) == {symbol, ADD_SYMBOL}:
            node.update(symbol, arithmetic_context)
            node = node.parent


@dataclass
class PPM:
    depth: int
    use_mask: bool = True
    update_rule: str = "C"
    lambda_context: ContextNode = field(init=False)

    def __post_init__(self) -> None:
        self.lambda_context = ContextNode(YASingArithmetic(BITS, False, self.update_rule))

    def encode(self, text: bytes, stream: OutBitStream) -> None:
        encode_node = self.lambda_context
        context = b""
        arithmetic_context = [ArithmeticContext(2 ** BITS - 1)]
        for symbol in text:
            update_node = _get_node_by_context(context, self.lambda_context)
            encode_node = _encode_with_node(symbol, update_node, stream, arithmetic_context, self.use_mask)
            update_contexts(arithmetic_context, context, encode_node, symbol, update_node, self.update_rule)
            context = (context + bytes([symbol]))[-self.depth:]
        encode_node.encode_finish(stream, arithmetic_context)

    def decode(self, length: int, encoded: bytes, stream: OutBitStream) -> None:
        decode_node = self.lambda_context
        context = b""
        arithmetic_context = [ArithmeticContext(2 ** BITS - 1)]
        for _ in range(length):
            update_node = _get_node_by_context(context, self.lambda_context)
            symbol, decode_node = _decode_with_node(encoded, update_node, stream, arithmetic_context, self.use_mask)
            update_contexts(arithmetic_context, context, decode_node, symbol, update_node, self.update_rule)
            context = (context + bytes([symbol]))[-self.depth:]
        decode_node.decode_finish(stream, arithmetic_context)


def _get_node_by_context(context: bytes, lambda_context: IContextNode) -> IContextNode:
    update_node = None
    for i in range(len(context)):
        update_node = get_node(context[i:], lambda_context)
        if update_node is not None:
            break
    return update_node or lambda_context


def get_node(context: bytes, context_node: IContextNode) -> t.Optional[IContextNode]:
    for symbol in context[::-1]:
        context_node = context_node.children.get(symbol)
        if context_node is None:
            break
    return context_node
