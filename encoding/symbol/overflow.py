from dataclasses import dataclass
from itertools import islice

from encoding.symbol.interface import SymbolEncoding
from infra.stream import InBitStream


@dataclass
class Overflow(SymbolEncoding):
    start_number: int = 16

    def encode(self, symbol: int) -> str:
        symbol -= self.start_number
        if symbol < 255:
            return bin(symbol)[2:].zfill(8)
        return "1" * 8 + bin(symbol - 255)[2:].zfill(16)

    def decode(self, stream: InBitStream) -> int:
        n = 0
        for bit in islice(stream.read(), 8):
            n *= 2
            n += int(bit)
        if n < 255:
            return n + self.start_number
        n = 0
        for bit in islice(stream.read(), 16):
            n *= 2
            n += int(bit)
        return n + self.start_number + 255

    def size(self, symbol: int) -> int:
        symbol -= self.start_number
        if symbol < 255:
            return 8
        return 24

    def __str__(self) -> str:
        return "С переполнением 8 + 16"


@dataclass
class OverflowOther(SymbolEncoding):
    def encode(self, symbol: int) -> str:
        if symbol < 255:
            return bin(symbol)[2:].zfill(8)
        symbol -= 255
        return "1" * 8 + bin(symbol)[2:].zfill(12)

    def decode(self, stream: InBitStream) -> int:
        n = 0
        for bit in islice(stream.read(), 8):
            n *= 2
            n += int(bit)
        if n < 255:
            return n
        n = 0
        for bit in islice(stream.read(), 12):
            n *= 2
            n += int(bit)
        return n + 255

    def size(self, symbol: int) -> int:
        if symbol < 255:
            return 8
        symbol -= 255
        return 20

    def __str__(self) -> str:
        return "С переполнением 8 + 12"
