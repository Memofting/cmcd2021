from dataclasses import dataclass
from itertools import islice
from math import log2

from encoding.symbol.interface import SymbolEncoding
from infra.stream import InBitStream


@dataclass
class Gamma(SymbolEncoding):
    start_number: int = 16

    def encode(self, symbol: int) -> str:
        symbol -= self.start_number - 1
        k = int(log2(symbol))
        return "0" * k + bin(symbol)[2:]

    def decode(self, stream: InBitStream) -> int:
        i = 0
        for i, bit in enumerate(stream.read()):
            if bit == '1':
                break
        n = 1
        for bit in islice(stream.read(), i):
            n *= 2
            n += int(bit)
        return n + self.start_number - 1

    def size(self, symbol: int) -> int:
        symbol -= self.start_number - 1
        return 2 * int(log2(symbol)) + 1

    def __str__(self) -> str:
        return "Гамма"
