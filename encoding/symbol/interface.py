import typing as t

from infra.stream import InBitStream, OutBitStream


class SymbolEncoding(t.Protocol):
    def encode(self, symbol: int) -> str:
        ...

    def decode(self, stream: InBitStream) -> int:
        ...

    def size(self, symbol: int) -> int:
        ...

    def __str__(self) -> str:
        ...

    def __repr__(self) -> str:
        return self.__str__()


class YASing(t.Protocol):
    def encode(self, symbol: int, stream: OutBitStream) -> None:
        ...

    def decode(self, encoded: bytes, stream: OutBitStream) -> int:
        ...
