from itertools import islice
from math import log2
from dataclasses import dataclass, field

from encoding.symbol.interface import SymbolEncoding
from encoding.symbol.gamma import Gamma
from infra.stream import InBitStream


@dataclass
class Delta(SymbolEncoding):
    start_number: int = 1
    _gamma: Gamma = field(default=Gamma(0), init=False)

    def encode(self, symbol: int) -> str:
        symbol -= self.start_number - 1
        k = int(log2(symbol))
        return self._gamma.encode(k) + bin(symbol)[3:]

    def decode(self, stream: InBitStream) -> int:
        k = self._gamma.decode(stream)
        n = 1
        for bit in islice(stream.read(), k):
            n *= 2
            n += int(bit)
        return n + self.start_number - 1

    def size(self, symbol: int) -> int:
        symbol -= self.start_number - 1
        return 2 * int(log2(symbol)) + 1

    def __str__(self) -> str:
        return "Дельта"
