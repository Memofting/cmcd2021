import argparse
from tasks import second, first, third, fourth

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers()

    first_query = subparsers.add_parser('first', help="Запускает решение для первой задачи")
    first_query.set_defaults(func=first.solution)

    second_query = subparsers.add_parser('second', help="Запускает решение для второй задачи")
    second_query.set_defaults(func=second.solution)

    third_query = subparsers.add_parser('third', help="Запускает решение для третьей задачи")
    third_query.set_defaults(func=third.solution)

    fourth_query = subparsers.add_parser('fourth', help="Запускает решение для четвёртой задачи")
    fourth_query.set_defaults(func=fourth.solution)

    options = parser.parse_args()
    options.func()
